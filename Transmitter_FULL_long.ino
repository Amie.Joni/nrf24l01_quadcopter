// -----------------------------------------------------------------------------
// this is the receiver
// this RF communication use nrf24l01 board
// -----------------------------------------------------------------------------

/*
  Pins:
    NRF24L01
      9=>CE
      10=>CS
      SCK=>13
      MOSI=>11
      MISO=>12
    LED
      2=>Green
      3=>Red
    BUZZ
      4=>Buzz
    Throttle=>A0  Yaw=>A1  Pitch=>A2  Roll=>A3 AUX1=>A4 AUX2=>8

    FixPin -> 5

*/
#include <SPI.h>
#include <RF24.h>

const int led_red = 3;
const int led_green = 2;
const int buzz = 4;
const int fixPin =5;
const int aux2Pin = 8;

struct MyData {
  byte throttle; byte yaw; byte pitch; byte roll; byte AUX1; byte AUX2;
};

MyData data;

byte addressComm[] = "D4A7711";
RF24 radio(9, 10);

void setup()
{
  Serial.begin(9600);
  pinMode(buzz,OUTPUT);
  pinMode(fixPin,INPUT_PULLUP);
  pinMode(aux2Pin,INPUT_PULLUP);

  //Configure the NRF24 module

  radio.begin();
  radio.setAutoAck(false);//1
  radio.setPALevel(RF24_PA_MAX);//2
  radio.setRetries(15,15);
  radio.setDataRate(RF24_250KBPS);
  radio.setChannel(108);//4
  radio.openWritingPipe(addressComm);
  radio.stopListening();

  resetData();

  //init bip
  digitalWrite(buzz,HIGH);
  delay(500);
  digitalWrite(buzz,LOW);
}

/**************************************************/

int correctValues(int val, int lower, int middle, int upper, bool reverse)
{
  val = constrain(val, lower, upper);

  if ( val < middle )
    val = map(val, lower, middle, 0, 128);
  else
    val = map(val, middle, upper, 128, 255);

  return ( reverse ? 255 - val : val );
}

//Smooth by using the average
int analogReadValues(int pin){
  int val = 0;
  for(int i=0;i<5;i++){
    val += analogRead(pin);
    delay(1);
  }
  return val/5;
}

void printAll(){
    Serial.print("Throttle:");Serial.print(data.throttle);Serial.print(" ");
    Serial.print("Yaw :");Serial.print(data.yaw);Serial.print(" ");
    Serial.print("Pitch :");Serial.print(data.pitch);Serial.print(" ");
    Serial.print("Roll :");Serial.print(data.roll);Serial.print(" ");
    Serial.print("Aux1 :");Serial.print(data.AUX1);Serial.print(" ");
    Serial.print("Aux2 :");Serial.println(data.AUX2);Serial.print(" ");
}

void loop()
{
  int fix_throttle = digitalRead(fixPin);

  //Calibration
  if(!fix_throttle) data.throttle = correctValues( analogReadValues(A0),0,511,1022, true );//updated if we are not fixing it
  data.yaw      = correctValues( analogReadValues(A1),0,503,1022, false );
  data.pitch    = correctValues( analogReadValues(A2),0,520,1021, true );
  data.roll     = correctValues( analogReadValues(A3),0,528,1021, true );
  data.AUX1     = correctValues( analogReadValues(A4),0,511,1022, true );
  data.AUX2     = digitalRead(8);

  //print readed to send info
  printAll();

  if ( radio.write(&data, sizeof(data)) ){
    delay(1);

  }

}

void resetData()
{
  //This are the start values of each channal

  data.throttle = 0;
  data.yaw = 127;
  data.pitch = 127;
  data.roll = 127;
  data.AUX1 = 0;//pot
  data.AUX2 = 0;
}
