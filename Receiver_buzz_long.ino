// -----------------------------------------------------------------------------
// this is the receiver
// this RF communication use nrf24l01 board
// -----------------------------------------------------------------------------

/*
    Pins :

    NRF24L01
      9=>CE
      10=>CS
      SCK=>13
      MOSI=>11
      MISO=>12

[By AmineJow]

*/

#include <SPI.h>
#include <RF24.h>
#include <Servo.h>

//buzzer
const int buzz = 8;
//  Led stats
const int ledRed = A1;
const int ledGreen = A0;

//Define widths
int pwm_width_2 = 0;
int pwm_width_3 = 0;
int pwm_width_4 = 0;
int pwm_width_5 = 0;
int pwm_width_6 = 0;
int pwm_width_7 = 0;

Servo PWM2;
Servo PWM3;
Servo PWM4;
Servo PWM5;
Servo PWM6;
Servo PWM7;

struct MyData {
  byte throttle; byte yaw; byte pitch; byte roll; byte AUX1; byte AUX2;
};

MyData data;

byte addressComm[] = "D4A7711";
RF24 radio(9, 10);

//used in landing safely if lost signal
unsigned long currentMillis; //will save the time since the board is on
unsigned long previousMillis = 0; //will be used in calculation
const long interval = 500; //the interval time to execute code

/**************************************************/

void setup()
{
   Serial.begin(9600);

  //Set the pins for each PWM signal
  PWM2.attach(2);
  PWM3.attach(3);
  PWM4.attach(4);
  PWM5.attach(5);
  PWM6.attach(6);
  PWM7.attach(7);

  //Leds
  pinMode(ledGreen,OUTPUT);
  pinMode(ledRed,OUTPUT);

  //buzzer
  pinMode(buzz,OUTPUT);

  //Configure the NRF24 module
  radio.begin();
  radio.setAutoAck(false);//1
  //radio.setPALevel(RF24_PA_MAX);//2
  //radio.setRetries(15,15);//3
  radio.setDataRate(RF24_250KBPS);
  radio.setChannel(108);
  radio.openReadingPipe(1,addressComm);
  radio.startListening();

  resetData();
}

/**************************************************/

unsigned long lastRecvTime = 0;

void loop()
{
  recvData();

  digitalWrite(buzz,LOW);

  //printAll();

  unsigned long now = millis();

  currentMillis = millis();

  //Here we check if we've lost signal, if we did we reset the values
  if ( now - lastRecvTime > 1000 ) {
    // Signal lost?
    resetData();
    digitalWrite(buzz,HIGH);
    digitalWrite(ledRed,HIGH);
    digitalWrite(ledGreen,LOW);
  }

pwm_width_2 = map(data.throttle, 0, 255, 2000, 1000);     //PWM value on digital pin D2
pwm_width_3 = map(data.yaw,      0, 255, 1000, 2000);     //PWM value on digital pin D3
pwm_width_4 = map(data.pitch,    0, 255, 1000, 2000);     //PWM value on digital pin D4
pwm_width_5 = map(data.roll,     0, 255, 2000, 1000);     //PWM value on digital pin D5
pwm_width_6 = map(data.AUX1,     0, 255, 2000, 1000);     //PWM value on digital pin D6
pwm_width_7 = map(data.AUX2,     0, 1, 1000, 2000);     //PWM value on digital pin D7

printAll1();

//Now we write the PWM signal using the servo function
PWM2.writeMicroseconds(pwm_width_2);
PWM3.writeMicroseconds(pwm_width_3);
PWM4.writeMicroseconds(pwm_width_4);
PWM5.writeMicroseconds(pwm_width_5);
PWM6.writeMicroseconds(pwm_width_6);
PWM7.writeMicroseconds(pwm_width_7);

}//Loop end

void recvData()
{
  if( radio.available() ){
    while ( radio.available() ) {
      radio.read(&data, sizeof(MyData));
      lastRecvTime = millis(); //here we receive the data
      //ON
      digitalWrite(ledRed,LOW);
      digitalWrite(ledGreen,HIGH);
    }
  }
}

void printAll(){
    Serial.print("Throttle:");Serial.print(data.throttle);Serial.print(" ");
    Serial.print("Yaw :");Serial.print(data.yaw);Serial.print(" ");
    Serial.print("Pitch :");Serial.print(data.pitch);Serial.print(" ");
    Serial.print("Roll :");Serial.print(data.roll);Serial.print(" ");
    Serial.print("Aux1 :");Serial.print(data.AUX1);Serial.print(" ");
    Serial.print("Aux2 :");Serial.println(data.AUX2);Serial.print(" ");
}

void printAll1(){
    Serial.print("Throttle:");Serial.print(pwm_width_2);Serial.print(" ");
    Serial.print("Yaw :");Serial.print(pwm_width_3);Serial.print(" ");
    Serial.print("Pitch :");Serial.print(pwm_width_4);Serial.print(" ");
    Serial.print("Roll :");Serial.print(pwm_width_5);Serial.print(" ");
    Serial.print("Aux1 :");Serial.print(pwm_width_6);Serial.print(" ");
    Serial.print("Aux2 :");Serial.println(pwm_width_7);Serial.print(" ");
}

void resetData()
{
  /*
//We define the inicial value of each data input
//2 potenciometers will be in the middle position so 127 is the middle from 254
 if (currentMillis - previousMillis >= interval) {//entre this section every 1s
   // save the last time
   previousMillis = currentMillis;
   //decrease throttle every 1s
   if(data.throttle >= 10){
     data.throttle-=5;

   }else data.throttle = 0;//import if we decrease less than 1000

   //Serial.println(data.throttle);
 }
 */
data.throttle = 255;
data.yaw = 127;
data.pitch = 127;
data.roll = 127;
data.AUX1 = 0;
data.AUX2 = 0;
}
/**************************************************/
